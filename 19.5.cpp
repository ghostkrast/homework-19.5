﻿#include <iostream>

class Animal
{
public:
void Voice()
    {
    std::cout << "animals! \n";
    }
};

class dog : public Animal
{
public:
  void Voice()
    {
        std::cout << "Woof! \n";
    }
};
class cat : public Animal
{
public:
    void Voice()
    {
      std::cout << "Meow! \n";
    }
};
class cow : public Animal
{
public:
    void Voice()
    {
        std::cout << "Mooo! \n";
    }
};

int main()
{   
    Animal temp;
    temp.Voice();
    dog* p = new dog;
    p->Voice();
    cat* p1 = new cat;
    p1->Voice();
    cow* p3 = new cow;
    p3->Voice();

}